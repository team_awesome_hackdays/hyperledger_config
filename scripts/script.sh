#!/bin/bash

CHANNEL_NAME="$1"
DELAY="$2"
LANGUAGE="$3"
: ${CHANNEL_NAME:="SwissHealthCareChannel"}
: ${TIMEOUT:="60"}
: ${LANGUAGE:="golang"}
LANGUAGE=`echo "$LANGUAGE" | tr [:upper:] [:lower:]`
COUNTER=1
MAX_RETRY=5
ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/intern.ranta.ch/msp/tlscacerts/tlsca.intern.ranta.ch-cert.pem

CC_SRC_PATH="github.com/chaincode/chaincode_example02/go/"
if [ "$LANGUAGE" = "node" ]; then
	CC_SRC_PATH="/opt/gopath/src/github.com/chaincode/chaincode_example02/node/"
fi

CHANNEL_PEER="intern.ranta.ch"
CHAIN_CODE_NAME="awesome"

echo
echo " ____    _____      _      ____    _____ "
echo "/ ___|  |_   _|    / \    |  _ \  |_   _|"
echo "\___ \    | |     / _ \   | |_) |   | |  "
echo " ___) |   | |    / ___ \  |  _ <    | |  "
echo "|____/    |_|   /_/   \_\ |_| \_\   |_|  "
echo
echo "  AdCubum HackDays Hyperledger Network"
echo "     (o:  Team Awesome ROCKS  :o)"
echo
echo " Channel name:" $CHANNEL_NAME
echo " Domain Base: " $CHANNEL_PEER
echo " Chain Code:  " $LANGUAGE
echo " Chain Name:  " $CHAIN_CODE_NAME
echo


# verify the result of the end-to-end test
verifyResult () {
	if [ $1 -ne 0 ] ; then
		echo "========= ERROR: Message ==========="
		echo $2
		echo "========= ERROR: FAILED to execute End-2-End Scenario ==========="
		echo
		exit 1
	fi
}

setGlobals () {
	CORE_PEER_LOCALMSPID=${1}
	CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/${2}/peers/peer0.${2}/tls/ca.crt
	CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/${2}/users/Admin@${2}/msp
	CORE_PEER_ADDRESS=${3}.${2}:7051
	env |grep CORE
}

createChannel() {
	setGlobals ${1} ${2} ${3}

	if [ -z "${CORE_PEER_TLS_ENABLED}" -o "${CORE_PEER_TLS_ENABLED}" = "false" ]; then
		peer channel create -o ${CHANNEL_PEER}:7050 -c ${CHANNEL_NAME} -f ./channel-artifacts/channel.tx >&log.txt
	else
		peer channel create -o ${CHANNEL_PEER}:7050 -c ${CHANNEL_NAME} -f ./channel-artifacts/channel.tx \
		--tls ${CORE_PEER_TLS_ENABLED} --cafile ${ORDERER_CA} >&log.txt
	fi
	res=$?
	#cat log.txt
	verifyResult $res "Channel creation failed"
	echo "===================== Channel \"${CHANNEL_NAME}\" is created successfully ===================== "
	echo
}

## Sometimes Join takes time hence RETRY at least for 5 times
joinWithRetry () {
	setGlobals ${1} ${2} ${3}

	peer channel join -b ${CHANNEL_NAME}.block >&log.txt
	res=$?
	#cat log.txt
	if [ $res -ne 0 -a ${COUNTER} -lt ${MAX_RETRY} ]; then
		COUNTER=` expr ${COUNTER} + 1`
		echo "PEER$1 failed to join the channel, Retry after 2 seconds"
		sleep $DELAY
		joinWithRetry ${1} ${2} ${3}
	else
		COUNTER=1
	fi
	verifyResult $res "After ${MAX_RETRY} attempts, ${1} ${3}.${2} has failed to Join ${CHANNEL_NAME}"
	echo "===================== ${1} ${3} joined channel \"${CHANNEL_NAME}\" ===================== "
	echo
}

updateAnchorPeers() {
	setGlobals ${1} ${2} ${3}

	if [ -z "${CORE_PEER_TLS_ENABLED}" -o "${CORE_PEER_TLS_ENABLED}" = "false" ]; then
		peer channel update -o ${CHANNEL_PEER}:7050 -c ${CHANNEL_NAME} -f ./channel-artifacts/${CORE_PEER_LOCALMSPID}anchors.tx >&log.txt
	else
		peer channel update -o ${CHANNEL_PEER}:7050 -c ${CHANNEL_NAME} -f ./channel-artifacts/${CORE_PEER_LOCALMSPID}anchors.tx \
		--tls ${CORE_PEER_TLS_ENABLED} --cafile ${ORDERER_CA} >&log.txt
	fi
	res=$?
	cat log.txt
	verifyResult $res "Anchor peer update failed"
	echo "===================== Anchor peers for org \"$CORE_PEER_LOCALMSPID\" on \"${CHANNEL_NAME}\" is updated successfully ===================== "
	sleep $DELAY
	echo
}


installChaincode () {
	setGlobals ${1} ${2} ${3}

	peer chaincode install -n ${CHAIN_CODE_NAME} -v 1.0 -l ${LANGUAGE} -p ${CC_SRC_PATH} >&log.txt
	res=$?
	#cat log.txt
	verifyResult $res "Chaincode installation on ${1} ${3} has Failed"
	echo "===================== Chaincode is installed on ${1} ${3} ===================== "
	echo
}

instantiateChaincode () {
	setGlobals ${1} ${2} ${3}

	# while 'peer chaincode' command can get the orderer endpoint from the peer (if join was successful),
	# lets supply it directly as we know it using the "-o" option
	if [ -z "${CORE_PEER_TLS_ENABLED}" -o "${CORE_PEER_TLS_ENABLED}" = "false" ]; then
		peer chaincode instantiate -o ${CHANNEL_PEER}:7050 -C ${CHANNEL_NAME} -n ${CHAIN_CODE_NAME} -l ${LANGUAGE} -v 1.0 \
		-c '{"Args":["init","a","100","b","200"]}' \
		-P "OR ('AdCubumMSP.member','AwesomeMSP.member','HealthCareMSP.member','GovernmentMSP.member')" >&log.txt
	else
		peer chaincode instantiate -o ${CHANNEL_PEER}:7050 -C ${CHANNEL_NAME} -n ${CHAIN_CODE_NAME} -l ${LANGUAGE} -v 1.0 \
		-c '{"Args":["init","a","100","b","200"]}' \
		-P "OR ('AdCubumMSP.member','AwesomeMSP.member','HealthCareMSP.member','GovernmentMSP.member')" \
		--tls ${CORE_PEER_TLS_ENABLED} --cafile ${ORDERER_CA} >&log.txt
	fi
	res=$?
	#cat log.txt
	verifyResult $res "Chaincode instantiation on ${1} ${3} has Failed"
	echo "===================== Chaincode is Instantiatiated on ${1} ${3} ===================== "
	echo
}

chaincodeQuery () {
	setGlobals ${1} ${2} ${3}

	echo "===================== Querying on PEER$PEER on channel '${CHANNEL_NAME}'... ===================== "
	local rc=1
	local starttime=$(date +%s)

	# continue to poll
	# we either get a successful response, or reach TIMEOUT
	while test "$(($(date +%s)-starttime))" -lt "$TIMEOUT" -a $rc -ne 0
	do
		sleep $DELAY
		echo "Attempting to Query ${1} ${3} for $(($(date +%s)-starttime)) secs"
		peer chaincode query -C ${CHANNEL_NAME} -n ${CHAIN_CODE_NAME} -c '{"Args":["query","a"]}' >&log.txt

		test $? -eq 0 && VALUE=$(cat log.txt | awk '/Query Result/ {print $NF}')
		test "$VALUE" = "$4" && let rc=0
	done
	echo
	#cat log.txt
	verifyResult $res "Query result on ${1} ${3} is INVALID"
	echo "===================== Query on ${1} ${3} is successful ===================== "
	fi
}

chaincodeInvoke () {
	setGlobals ${1} ${2} ${3}

	if [ -z "${CORE_PEER_TLS_ENABLED}" -o "${CORE_PEER_TLS_ENABLED}" = "false" ]; then
		peer chaincode invoke -o ${CHANNEL_PEER}:7050 -C ${CHANNEL_NAME} -n ${CHAIN_CODE_NAME} \
		-c '{"Args":["invoke","a","b","10"]}' >&log.txt
	else
		peer chaincode invoke -o ${CHANNEL_PEER}:7050 -C ${CHANNEL_NAME} -n ${CHAIN_CODE_NAME} \
		-c '{"Args":["invoke","a","b","10"]}' \
		--tls ${CORE_PEER_TLS_ENABLED} --cafile ${ORDERER_CA} >&log.txt
	fi
	res=$?
	#cat log.txt
	verifyResult $res "Invoke execution on ${1} ${3} is failed"
	echo "===================== Invoke transaction on ${1} ${3} is successful ===================== "
	echo
}

## Create channel
echo "Creating channel ${CHANNEL_NAME}..."
createChannel "AwesomeMSP" "awesome.intern.ranta.ch" "peer0"

## Join all the peers to the channel
echo "Join all Peers to the channel..."
joinWithRetry "AwesomeMSP" "awesome.intern.ranta.ch" "peer0"
joinWithRetry "AwesomeMSP" "awesome.intern.ranta.ch" "peer1"
joinWithRetry "AwesomeMSP" "awesome.intern.ranta.ch" "peer2"
joinWithRetry "AwesomeMSP" "awesome.intern.ranta.ch" "peer3"
joinWithRetry "AwesomeMSP" "awesome.intern.ranta.ch" "peer4"

joinWithRetry "AdCubumMSP" "adc.intern.ranta.ch" "peer0"
joinWithRetry "AdCubumMSP" "adc.intern.ranta.ch" "peer1"
joinWithRetry "AdCubumMSP" "adc.intern.ranta.ch" "peer2"
joinWithRetry "AdCubumMSP" "adc.intern.ranta.ch" "peer3"

joinWithRetry "HealthCareMSP" "healthcare.intern.ranta.ch" "peer0"
joinWithRetry "HealthCareMSP" "healthcare.intern.ranta.ch" "peer1"
joinWithRetry "HealthCareMSP" "healthcare.intern.ranta.ch" "peer2"

joinWithRetry "GovernmentMSP" "gov.intern.ranta.ch" "peer0"
joinWithRetry "GovernmentMSP" "gov.intern.ranta.ch" "peer1"


# Set the anchor peers for each org in the channel
echo "Updating anchor peers..."
updateAnchorPeers "AwesomeMSP" "awesome.intern.ranta.ch" "peer0"
updateAnchorPeers "AdCubumMSP" "adc.intern.ranta.ch" "peer0"
updateAnchorPeers "HealthCareMSP" "healthcare.intern.ranta.ch" "peer0"
updateAnchorPeers "GovernmentMSP" "gov.intern.ranta.ch" "peer0"


# Install chaincode on every peer0 on all organisations
echo "Installing chaincode on org1/peer0..."
installChaincode "AwesomeMSP" "awesome.intern.ranta.ch" "peer0"
installChaincode "AdCubumMSP" "adc.intern.ranta.ch" "peer0"
installChaincode "HealthCareMSP" "healthcare.intern.ranta.ch" "peer0"
installChaincode "GovernmentMSP" "gov.intern.ranta.ch" "peer0"


# Instantiate chaincode on Awesome/Peer0
echo "Instantiating chaincode on Aewsome peer0..."
instantiateChaincode "AwesomeMSP" "awesome.intern.ranta.ch" "peer0"


# Query on chaincode
echo "Querying chaincode on org1/peer0..."
chaincodeQuery "AdCubumMSP" "adc.intern.ranta.ch" "peer3" 100

# Invoke on chaincode
echo "Sending invoke transaction on Government Peer1..."
chaincodeInvoke "GovernmentMSP" "gov.intern.ranta.ch" "peer1"

echo
echo "========= All GOOD, Network execution completed =========== "
echo
echo " Channel name:" $CHANNEL_NAME
echo " Domain Base: " $CHANNEL_PEER
echo " Chain Code:  " $LANGUAGE
echo " Chain Name:  " $CHAIN_CODE_NAME
echo "=========================================================== "
echo

exit 0
