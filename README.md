# HyperLedger Base Configuration for Docker

Base configuration for a HyperLedger Setup with Docker.

## Configuration

```bash
$ cat env.vars >> ~/.bashrc
```
and reopen the bash.

## Setup Hyperledger

Only once run the Script `build.sh` after final configuration.


