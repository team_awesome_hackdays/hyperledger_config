#!/bin/sh
export FABRIC_CFG_PATH=${PWD}
export CHANNEL_NAME=awesome

mkdir channel-artifacts

# Create the configuration
cryptogen generate --config=./crypto-config.yaml
configtxgen -profile SwissHealthCareGenesis -outputBlock ./channel-artifacts/genesis.block

# Create the main Channel
configtxgen -profile SwissHealthCareChannel -outputCreateChannelTx ./channel-artifacts/channel.tx -channelID ${CHANNEL_NAME}

# Create a Channel for each company
for C in AdCubum Awesome HealthCare Government; do
    configtxgen -profile SwissHealthCareChannel -outputAnchorPeersUpdate ./channel-artifacts/${C}MSPanchors.tx -channelID ${CHANNEL_NAME} -asOrg ${C}MSP
done
